#!/bin/bash
set -ax

exec /opt/schema-registry/bin/schema-registry-start /schema-registry.properties
