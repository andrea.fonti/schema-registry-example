# How to run

`docker-compose build`
`docker-compose up`

# How to stop

`docker-compose down -v`

# How to execute the sample request

The sample request is performed by this script

```
schema-registry/run-request.sh
```

that is copied to

```
/run-request.sh
```

inside the docker image

to perform the request execute directly that script inside the container

```bash
➜ docker-compose exec schema-registry /run-request.sh
```

or jump inside the schema registry container and execute it from there

```bash
➜ docker-compose exec schema-registry bash
root@schema-registry:/# ./run-request.sh
```

The output should be something similar to this

```
+ curl -X POST localhost/subjects/Platfrm-mstest001-ProvaDummy-value/versions -H 'accept: application/vnd.schemaregistry.v1+json' -H 'Content-Type: application/vnd.schemaregistry.v1+json' -d '{
   "schemaType": "AVRO",
   "references": [
   ],
   "schema": "{\"fields\": [{\"name\": \"name\",\"type\": \"string\" }, {\"name\": \"surname\",\"type\": \"string\"}],\"name\": \"Payload\",\"namespace\": \"com.enel.platform.common.model\",\"type\": \"record\"}"
 }'
{"id":1}
```

